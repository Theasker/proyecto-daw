angular.module('masomenosApp.menu', ['firebase'])

.controller('MenuCtrl',['$scope','fbURL','$state','CommonProp','$firebaseAuth',function($scope,fbURL,$state,CommonProp,$firebaseAuth) {
	var ref = new Firebase(fbURL);
	var loginObj = $firebaseAuth(ref);
	$scope.user = {};
	$scope.user.email = CommonProp.getUser();
	
	// Guardo en un Factory los datos del usuario para poder usralos después =======
	if (loginObj.$getAuth()){
		var usuario = loginObj.$getAuth().password.email;
		CommonProp.setUser(usuario);
		$scope.user.email = usuario;
	}
	$scope.Logout = function(){
		ref.unauth();
		$state.go("login");
	};
	
}]);