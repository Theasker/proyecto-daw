var app = angular.module('masomenosApp', [
    'ui.router',
    'masomenosApp.login',
    'masomenosApp.menu',
    'masomenosApp.fechas',
    'masomenosApp.home',
    'masomenosApp.estadistica']);
    
// Url de la base de datos de Firebase
//app.value('fbURL', 'https://theaskerpruebas.firebaseio.com/');
app.value('fbURL', 'https://theaskerdaw.firebaseio.com/');

// Control de los errores de acceso y en caso de error vuelvo al login
app.run(['$rootScope','$state',function($rootScope,$state){
	$rootScope.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams, error){
		// We can catch the error thrown when the $requireAuth promise is rejected
	  // and redirect the user back to the home page
	  if (error === "AUTH_REQUIRED") {
	  	console.log(error);
	    $state.go("login");
	  }
	});
}]);

app.factory("Auth", ["$firebaseAuth",'fbURL',function($firebaseAuth,fbURL) {
    var ref = new Firebase(fbURL);
    return $firebaseAuth(ref);
  }
]);

// Usuario logueado
app.service('CommonProp',function(){
	var user = '';
	var ultimaSeleccion = {};
	return {
		getUser: function() {return user;},
		setUser: function(value) {user = value;},
	  // Usuario cambiados los puntos por "*"
		getUserSinPuntos: function(){
		  if(user.indexOf('.') !== -1){
    	  return user.split('.').join('*');
    	}
		},
		setSeleccion: function(seleccion) {ultimaSeleccion = seleccion;},
		getSeleccion: function() {return ultimaSeleccion;}
   	};
});

// Enrutado 
app.config(['$stateProvider','$urlRouterProvider',function ($stateProvider,$urlRouterProvider){
	$urlRouterProvider.otherwise('/home');
	$stateProvider
		.state('login',{
			url: '/login',
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl',
			resolve: {
		    // controller will not be loaded until $waitForAuth resolves
		    // Auth refers to our $firebaseAuth wrapper in the example above
		    "currentAuth": ["Auth", function(Auth) {
	      // $waitForAuth returns a promise so the resolve waits for it to complete
	      	return Auth.$waitForAuth();
    		}]
  		}
		})
		.state('home',{
			url: '/home',
			views:{
				'menu':{
					templateUrl: 'views/menu.html',
					controller: 'MenuCtrl'
				},
				'lateral':{
					templateUrl: 'views/lateral.html',
					controller: 'FechasCtrl'
				},
				'main':{
					templateUrl: 'views/inicio.html',
					controller: 'HomeCtrl'
				}		
			},
			resolve: {
		    // controller will not be loaded until $requireAuth resolves
		    // Auth refers to our $firebaseAuth wrapper in the example above
		    "currentAuth": ["Auth", function(Auth) {
		      // $requireAuth returns a promise so the resolve waits for it to complete
		      // If the promise is rejected, it will throw a $stateChangeError (see above)
		      return Auth.$requireAuth();
	    	}]
			}
		})
		.state('dias',{
			url: '/dias/:anio?mes?semana',
			views:{
				'menu':{
					templateUrl: 'views/menu.html',
					controller: 'MenuCtrl'
				},
				'lateral':{
					templateUrl: 'views/lateral.html',
					controller: 'FechasCtrl'
				},
				'main':{
					templateUrl: 'views/inicio.html',
					controller: 'HomeCtrl'
				}		
			},
			resolve: {
		    "currentAuth": ["Auth", function(Auth) {
		      return Auth.$requireAuth();
	    	}]
			}
		})
		.state('estadistica',{
			url: '/estadistica/:anio',
			views:{
				'menu':{
					templateUrl: 'views/menu.html',
					controller: 'MenuCtrl'
				},
				'estadistica':{
					templateUrl: 'views/estadistica.html',
					controller: 'EstadisticaCtrl'
				}		
			},
			resolve: {
		    "currentAuth": ["Auth", function(Auth) {
		      return Auth.$requireAuth();
	    	}]
			}
		});
}]);