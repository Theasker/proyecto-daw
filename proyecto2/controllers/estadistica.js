var app = angular.module('masomenosApp.estadistica', ['firebase']);

app.controller('EstadisticaCtrl',['$scope','fbURL','$stateParams','CommonProp','$firebaseAuth','$firebaseArray','$firebaseObject','$state','currentAuth',function($scope,fbURL,$stateParams,CommonProp,$firebaseAuth,$firebaseArray,$firebaseObject,$state,currentAuth) {
	
	var ref = new Firebase(fbURL);
	var usuarioSinPuntos = CommonProp.getUserSinPuntos(); // Usuario sin puntos

	$scope.anio = $stateParams.anio;

	$scope.estadisticas = function(){
		var refAnio = $firebaseObject(ref.child(usuarioSinPuntos).child('years').child($scope.anio));
		var estadistica = [];
		var labels = [];
		var cont = 0;
		refAnio.$loaded().then(function(x){
			for (var mes = 0; mes < 12; mes++){
				var ingreso = 0;
				var gasto = 0;
				
				// Recorremos las semanas del mes
				for (var semana = 0; semana < refAnio[mes].length; semana++){

          if(refAnio[mes][semana].dias){ // Hay datos de ingresos y gastos
            // dias de la semana
            for(var dia = 0; dia < refAnio[mes][semana].dias.length; dia++){
              //console.log(refAnio[mes][semana].dias[dia]);
              
              // Generación aleatoria de los gastos e ingresos
              /*var refobjDia = ref.child(usuarioSinPuntos).child('years').child($scope.anio).child(mes).child(semana).child('dias').child(dia);
						  var objDia = $firebaseObject(refobjDia);
						
						  objDia.$loaded().then(function(x){
						    objDia.ingreso = Math.floor((Math.random() * 300) + 1);
						    //objDia.gastos = {};
						    objDia.totalGastos = Math.floor((Math.random() * 100) + 1);
						    objDia.$save();
						  });*/
						  // FIN Generación de ingresos y gastos ========================================

              ingreso = ingreso + refAnio[mes][semana].dias[dia].ingreso;

              if(refAnio[mes][semana].dias[dia].totalGastos){
              	gasto = gasto + refAnio[mes][semana].dias[dia].totalGastos;
              }
            }
            console.log(ingreso,gasto);
          }else{
              
          }
        }

        estadistica.push(ingreso-gasto);
        cont++;
        labels.push(cont);
        
      }

      console.log(estadistica.length,estadistica);
      console.log(labels.length,labels);


      var data = {
      	labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
      	datasets: [
      	{
      		label: "My First dataset",
      		fillColor: "rgba(151,187,205,0.2)",
      		strokeColor: "rgba(151,187,205,1)",
      		pointColor: "rgba(151,187,205,1)",
      		pointStrokeColor: "#fff",
      		pointHighlightFill: "#fff",
      		pointHighlightStroke: "rgba(151,187,205,1)",
      		data: estadistica
      	}
      	]
      };

     /* var options = Chart.defaults.global = {
      	responsive: true,
      	maintainAspectRatio: true  
      };*/

      var options = {

		    ///Boolean - Whether grid lines are shown across the chart
		    scaleShowGridLines : true,

		    //String - Colour of the grid lines
		    scaleGridLineColor : "rgba(0,0,0,.05)",

		    //Number - Width of the grid lines
		    scaleGridLineWidth : 1,

		    //Boolean - Whether to show horizontal lines (except X axis)
		    scaleShowHorizontalLines: true,

		    //Boolean - Whether to show vertical lines (except Y axis)
		    scaleShowVerticalLines: true,

		    //Boolean - Whether the line is curved between points
		    bezierCurve : true,

		    //Number - Tension of the bezier curve between points
		    bezierCurveTension : 0.4,

		    //Boolean - Whether to show a dot for each point
		    pointDot : true,

		    //Number - Radius of each point dot in pixels
		    pointDotRadius : 4,

		    //Number - Pixel width of point dot stroke
		    pointDotStrokeWidth : 1,

		    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		    pointHitDetectionRadius : 20,

		    //Boolean - Whether to show a stroke for datasets
		    datasetStroke : true,

		    //Number - Pixel width of dataset stroke
		    datasetStrokeWidth : 2,

		    //Boolean - Whether to fill the dataset with a colour
		    datasetFill : true,

		    //String - A legend template
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
		    responsive: true,
		    maintainAspectRatio: true 

		};

      var context = document.getElementById('skills').getContext('2d');
      var skillsChart = new Chart(context).Line(data,options);

    });
	};

	$scope.estadisticasVirtuales = function(){
		var estadistica = [];
		var labels = [];
		var cont = 0; // Contador de semanas
		var anio = calculoSemanas($stateParams.anio);
		var objAnio = $firebaseObject(ref.child(usuarioSinPuntos).child('years').child($scope.anio));
		console.log(objAnio);
		objAnio.$loaded().then(function(x){
			for (var mes = 0; mes < 12;mes++){
				var ingresos = 0;
				var gastos = 0;
				//console.log(objAnio[mes]);
				for (var semana = 0;semana < objAnio[mes].length;semana++){
					for (var dia = 0;dia < objAnio[mes][semana].dias.length;dia++){
						var ingreso = Math.floor((Math.random() * 300) + 1);
						var gasto = Math.floor((Math.random() * 100) + 1);
						objAnio[mes][semana].dias[dia].ingreso = ingreso;
						objAnio[mes][semana].dias[dia].totalGastos = gasto;
						objAnio.$save();
						cont++;
						//console.log(objAnio[mes][semana].dias[dia]);
						ingresos = ingresos + ingreso;
						gastos = gastos + gasto;
					} 
				}
				console.log(ingresos,gastos); 
				estadistica.push(ingresos - gastos);
				cont++;
				labels.push(cont);
			}
			estadisticar(); 
		});
		
		function estadisticar(){
			console.log(estadistica.length,estadistica);
	    console.log(labels.length,labels);
			var data = {
	      	labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
	      	datasets: [
	      	{
	      		label: "My First dataset",
	      		fillColor: "rgba(151,187,205,0.2)",
	      		strokeColor: "rgba(151,187,205,1)",
	      		pointColor: "rgba(151,187,205,1)",
	      		pointStrokeColor: "#fff",
	      		pointHighlightFill: "#fff",
	      		pointHighlightStroke: "rgba(151,187,205,1)",
	      		data: estadistica
	      	}
	      	]
	      };
	
	     /* var options = Chart.defaults.global = {
	      	responsive: true,
	      	maintainAspectRatio: true  
	      };*/
	
	      var options = {
	
			    ///Boolean - Whether grid lines are shown across the chart
			    scaleShowGridLines : true,
	
			    //String - Colour of the grid lines
			    scaleGridLineColor : "rgba(0,0,0,.05)",
	
			    //Number - Width of the grid lines
			    scaleGridLineWidth : 1,
	
			    //Boolean - Whether to show horizontal lines (except X axis)
			    scaleShowHorizontalLines: true,
	
			    //Boolean - Whether to show vertical lines (except Y axis)
			    scaleShowVerticalLines: true,
	
			    //Boolean - Whether the line is curved between points
			    bezierCurve : true,
	
			    //Number - Tension of the bezier curve between points
			    bezierCurveTension : 0.4,
	
			    //Boolean - Whether to show a dot for each point
			    pointDot : true,
	
			    //Number - Radius of each point dot in pixels
			    pointDotRadius : 4,
	
			    //Number - Pixel width of point dot stroke
			    pointDotStrokeWidth : 1,
	
			    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			    pointHitDetectionRadius : 20,
	
			    //Boolean - Whether to show a stroke for datasets
			    datasetStroke : true,
	
			    //Number - Pixel width of dataset stroke
			    datasetStrokeWidth : 2,
	
			    //Boolean - Whether to fill the dataset with a colour
			    datasetFill : true,
	
			    //String - A legend template
			    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
			    responsive: true,
			    maintainAspectRatio: true 
	
			};
	
	      var context = document.getElementById('skills').getContext('2d');
	      var skillsChart = new Chart(context).Line(data,options);
		}
		
	};
	
	// Genera los ingresos y gastos de un año para estadísticas
	// De forma estática en un array (para pruebas).
	function generarEstadisticasAnio(anio){
		for (var mes = 0; mes < 12;mes++){
			for (var semana = 0;semana < anio[mes].length;semana++){
				anio[mes][semana].dias = crearDias(anio[mes][semana].desde,anio[mes][semana].hasta);
				console.log(anio[mes][semana].dias);
				for(var dia = 0;dia < anio[mes][semana].dias.length;dia++){
					anio[mes][semana].dias[dia].ingreso = Math.floor((Math.random() * 300) + 1);
					anio[mes][semana].dias[dia].totalGastos = Math.floor((Math.random() * 100) + 1);
				}
			}
		}
	}
	
	// Crea las semanas de un año dado
	function calculoSemanas(anio){
		// Establecemos el primer día del año
		// Los parámetros son año, mes(0-11), día, horas, minutos, segundos (sólo usamos año, mes y día)
		dt = new Date(anio,0,1); // yyyy/mm/dd -> yyyy/01/01
	
		var dayweek = dt.getDay();
		var month = dt.getMonth();
		var day = dt.getDate();
		var year = dt.getFullYear();
		var semanas = []; // Array con los datos de todas las semanas
		//var semanas = {year: year}; // Objeto con los datos de todas las semanas
		var desde = dt.getTime();
		var hasta;
		// Asigno el mes a otra variable para poder controlar en cambio del mismo
		mesAnterior = month; // Cambiamos de mes
		semanas.push(new Array()); // Agregamos el mes al array
		while(year == dt.getFullYear()){
			day++; // Sumo un día
			month = dt.getMonth(); // Obtengo el mes de esta fecha
			dt.setDate(day); // Asigno el día sumado 
			day = dt.getDate(); // Obtenemos el día de la fecha
			// Si el día es 1 (lunes) lo guardamos en 'desde'
			// para saber el día que comienza la semana
			if (dt.getDay() == 1){ // El día 1 es el lunes
				desde = dt.getTime();
			}
			// Si el día es 0 (domingo) o final de año ( 31/11 -> 11 es diciembre) lo guardamos en 'hasta' 
			// para saber el día final de semana
			if ( (dt.getDay() === 0) || ( (dt.getMonth() == 11) && (dt.getDate() == 31) ) ) {
				// Controlamos el cambio del mes
				if (month > mesAnterior){
					mesAnterior = month; // Cambiamos de mes
					semanas.push(new Array()); // Agregamos el mes al array
				}
				hasta = dt.getTime();
				//semanas.meses = {('mes'+month): {desde: desde, hasta: hasta}};
				// Creo una variable Date para poder sacar dd/mm/yyyy
				desde2 = new Date(desde);
				hasta2 = new Date(hasta);
				
				semanas[month].push({
					//desde: desde2.getDate()+"/"+desde2.getMonth()+"/"+desde2.getFullYear(),
					//hasta: hasta2.getDate()+"/"+hasta2.getMonth()+"/"+hasta2.getFullYear()
					desde: desde, hasta: hasta, dias: {}
				});
				
			}
		}
		return semanas;
	}
	
	// Crea los días de la semana
  function crearDias(desde,hasta){
    var dias = [];
    //var dia = {};
    //var fecha = "";
    desde = new Date(desde); // Obtengo la fecha del timestamp de desde
    //hasta = new Date(hasta); // Obtengo la fecha del timestamp de hasta
    var hoydia = desde.getDate(); // Date del día desde que iré sumando durante la semana
    //console.log('hasta: ',hasta);
    var hoyts = desde;
    //console.log(hoyts.getTime());
    while (hoyts.getTime() <= hasta){
      //fecha = (hoyts.getDate()+"/"+hoyts.getMonth()+"/"+hoyts.getFullYear());
      dias.push( {dia: hoyts.getTime(), ingreso: 0.00, totalGastos: 0.00} );
      hoydia++;
      hoyts.setDate(hoydia);
      hoydia = hoyts.getDate();
    }
    return dias;
  }

}]);