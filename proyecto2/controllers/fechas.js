var app = angular.module('masomenosApp.fechas', ['firebase']);

app.controller('FechasCtrl',['$scope','fbURL','$firebaseAuth','CommonProp','$state','$stateParams','$firebaseArray','$firebaseObject',function($scope,fbURL,$firebaseAuth,CommonProp,$state,$stateParams,$firebaseArray,$firebaseObject) {
	var ref = new Firebase(fbURL);
	var usuarioSinPuntos = CommonProp.getUserSinPuntos();

	var arrayMes=[
    {id:0, mes:"Enero"},
    {id:1, mes:"Febrero"},
    {id:2, mes:"Marzo"},
    {id:3, mes:"Abril"},
    {id:4, mes:"Mayo"},
    {id:5, mes:"Junio"},
    {id:6, mes:"Julio"},
    {id:7, mes:"Agosto"},
    {id:8, mes:"Septiembre"},
    {id:9, mes:"Octubre"},
    {id:10, mes:"Noviembre"},
    {id:11, mes:"Diciembre"}    
  ];

	$scope.year = {};
	$scope.year.mensaje = "";
	$scope.year.btnShowYearForm = true;
  $scope.year.showYearForm = false;
  
  document.getElementById("anio").focus();

	// Si se ha seleccionado una semana y estamos en el estado 'dias'
	if ($stateParams.anio){
		//console.log('parámetros',$stateParams);
		var seleccion = {
			anio: $stateParams.anio,
			mes: arrayMes[$stateParams.mes],
			semana: $stateParams.semana
		};
		//console.log(seleccion);
		CommonProp.setSeleccion(seleccion);
		$scope.year.anioSeleccionado = seleccion.anio;
		$scope.year.mesSeleccionado = seleccion.mes;
		$scope.year.semanaSeleccionada = seleccion.semana;
		mostrarSemanas($scope.year.mesSeleccionado.id);
	}else{ // Sino compruebo que hay selección de año, mes y semana en la base de datos
		var seleccionObj = $firebaseObject(ref.child( usuarioSinPuntos ).child('datos').child('seleccion'));
		seleccionObj.$loaded().then(function(x){
			if (seleccionObj.anio){ // SI que hay seleccion de año,mes y semana en la base de datos
				console.log("cargando seleccion de bbdd");
				//console.log(seleccionObj);
				CommonProp.setSeleccion(seleccionObj);
				$scope.year.anioSeleccionado = seleccionObj.anio;
				$scope.year.mesSeleccionado = seleccionObj.mes;
				$scope.year.semanaSeleccionada = seleccionObj.semana;
				mostrarSemanas($scope.year.mesSeleccionado.id);
			}else{ // No hay nada seleccionado y mostramos los años
				crearInicio();
				comprobarAnios();
			}
		});
	}

	// Comprobación/creación si el inicio del arbol de un usuario si está creado
	function crearInicio(){
		var inicioArbol = $firebaseArray(ref.child(usuarioSinPuntos).child('datos'));
		// Promesa para saber cuando se ha cargado el arbol del usuario
		inicioArbol.$loaded().then(function(){
			// Si no hay datos en el arbol de inicio del usuario lo creamos con los datos de autenticación
			if (inicioArbol.length == 0){ 
				var autenticado = $firebaseAuth(ref).$getAuth();
				var usuario = {
					email: autenticado.password.email,
					uid: autenticado.uid,
					token: autenticado.token
				};
				ref.child(usuarioSinPuntos).child('datos').set(usuario); // Agreghamos los datos

			}
		})
	}

	$scope.year.comprobarAnios = function(){
		comprobarAnios();
	};
	
	// Cargo los años dados de alta si los hay
	function comprobarAnios(){
		var treeYears = $firebaseArray(ref.child(usuarioSinPuntos).child('years'));
		treeYears.$loaded().then(function(){
			if(treeYears.length !== 0){ // Hay años agregados
				$scope.year.mensaje = "";
				$scope.year.anios = treeYears;
			}else{ // No hay años dados de alta
				$scope.year.showMensaje = true;
				$scope.year.mensaje = "Introduce algún año";
			}
			mostrarAnios();
		});
	}

	// Visualiza el div de los años y oculta el resto
	function mostrarAnios(){
		$scope.year.showYears = true;
		$scope.year.showYearsDelete = false;
		$scope.year.showMeses = false;
		$scope.year.showSemanas = false;
	}

	// Crea un nuevo año
	$scope.year.newYear = function(year){
		var anio = $firebaseArray(ref.child(usuarioSinPuntos).child('years').child(year));
		// Comprobarmos si existe
		anio.$loaded().then(function(x){

			if (anio.length == 0){ // no existe el año
				if ($scope.year.year < 1970){ // año demasiado antigüo
					$scope.year.mensaje = "No se pueden introducir años antes de 1.970.";
				}else{ // año correto.
					ref.child(usuarioSinPuntos).child('years').child(year).set(calculoSemanas(year));
					$scope.year.showYearForm = false;
					$scope.year.btnShowYearForm = true;
					comprobarAnios();
				}
				$scope.year.year = "";
			}else { // Año duplicado
				console.log("El año "+$scope.year.year+" ya existe y no se puede duplicar.");
				$scope.year.showMensaje = true
				$scope.year.mensaje = "El año "+$scope.year.year+" ya existe y no se puede duplicar.";
			} 

		});		
	};

	// Crea las semanas de un año dado
	function calculoSemanas(anio){
		// Establecemos el primer día del año
		// Los parámetros son año, mes(0-11), día, horas, minutos, segundos (sólo usamos año, mes y día)
		dt = new Date(anio,0,1); // yyyy/mm/dd -> yyyy/01/01
	
		var dayweek = dt.getDay();
		var month = dt.getMonth();
		var day = dt.getDate();
		var year = dt.getFullYear();
		var semanas = []; // Array con los datos de todas las semanas
		//var semanas = {year: year}; // Objeto con los datos de todas las semanas
		var desde = dt.getTime();
		var hasta;
		// Asigno el mes a otra variable para poder controlar en cambio del mismo
		mesAnterior = month; // Cambiamos de mes
		semanas.push(new Array()); // Agregamos el mes al array
		while(year == dt.getFullYear()){
			day++; // Sumo un día
			month = dt.getMonth(); // Obtengo el mes de esta fecha
			dt.setDate(day); // Asigno el día sumado 
			day = dt.getDate(); // Obtenemos el día de la fecha
			// Si el día es 1 (lunes) lo guardamos en 'desde'
			// para saber el día que comienza la semana
			if (dt.getDay() == 1){ // El día 1 es el lunes
				desde = dt.getTime();
			}
			// Si el día es 0 (domingo) o final de año ( 31/11 -> 11 es diciembre) lo guardamos en 'hasta' 
			// para saber el día final de semana
			if ( (dt.getDay() === 0) || ( (dt.getMonth() == 11) && (dt.getDate() == 31) ) ) {
				// Controlamos el cambio del mes
				if (month > mesAnterior){
					mesAnterior = month; // Cambiamos de mes
					semanas.push(new Array()); // Agregamos el mes al array
				}
				hasta = dt.getTime();
				//semanas.meses = {('mes'+month): {desde: desde, hasta: hasta}};
				// Creo una variable Date para poder sacar dd/mm/yyyy
				desde2 = new Date(desde);
				hasta2 = new Date(hasta);
				
				semanas[month].push({
					//desde: desde2.getDate()+"/"+desde2.getMonth()+"/"+desde2.getFullYear(),
					//hasta: hasta2.getDate()+"/"+hasta2.getMonth()+"/"+hasta2.getFullYear()
					desde: desde, hasta: hasta, dias: {}
				});
				
			}
		}
		return semanas;
	}

	// Confirmación del borrado de un año
	$scope.year.borrarAnioConfirmacion = function(anio){
		$scope.year.showYears = false;
		$scope.year.showYearsDelete = true;

		$scope.year.showSemanas = false;
		$scope.year.yearDelete = anio; // Año que hay que confirmar su borrado
	};
	
	// Borro un año
	$scope.year.borrarAnio = function(year){
		var refAnio = new Firebase(fbURL + usuarioSinPuntos + '/years/'+ year);
		refAnio.remove(function(error){
			if (error) {
		    console.log('El año '+year+' NO se ha borrado.',error);
		  } else {
		  	console.log('El año '+year+' ha sido borrado.');
		  	eliminarSeleccion(year);
		  }
		  comprobarAnios();
		});
	};
	
	// Eliminar la selección de año, mes y semana si corresponde con el año a borrar
	function eliminarSeleccion(anio){
		var seleccion = $firebaseObject(ref.child( usuarioSinPuntos ).child('datos').child('seleccion'));
		seleccion.$loaded().then(function(x){
			// Compruebo que hay datos y coincide con el año a borrar
			console.log("eliminarSeleccion: ",seleccion.anio,anio);
			if (seleccion.anio == anio){
				seleccion.$remove().then(function(x){
					$state.go('home');
				});
			}
		}).catch(function(error){console.log('NO se ha podido cargar la seleccion.');});
	}

	// Mostrar los meses de un año pulsado
	$scope.year.mostrarMeses = function(anio){
		$scope.year.anioSeleccionado = anio; // Guardo el año en que se ha hecho click
		$scope.year.meses = arrayMes; // Asocio el objeto a los datos del select
		
		$scope.year.showYears = false;
		$scope.year.showYearsDelete = false;
		$scope.year.showMeses = true;
		$scope.year.showSemanas = false;
	};

	$scope.year.mostrarSemanas = function(mes){
		mostrarSemanas(mes);
	};

	// Muestra las semanas del mes seleccionado
	function mostrarSemanas(mes){
		$scope.year.showYears = false;
		$scope.year.showYearsDelete = false;
		$scope.year.showMeses = false;
		$scope.year.showSemanas = true;

		var anio = $scope.year.anioSeleccionado;
		//console.log(anio,mes);
		$scope.year.semanas = $firebaseArray(ref.child(usuarioSinPuntos).child('years').child(anio).child(mes));
	}

		// Muestra los días de la semana seleccionada
	$scope.year.mostrarDias = function(idSemana){
		var seleccion = {
			anio: $scope.year.anioSeleccionado,
			mes: $scope.year.mesSeleccionado, // {id: 3, mes: "Abril"}
			semana: idSemana
		};
		$scope.year.seleccion = seleccion;
		CommonProp.setSeleccion(seleccion);
		
		$scope.year.semanaSeleccionada = idSemana; // Semana seleccionada
		// Guardo la seleccion de año, mes y semana en la base de datos.
		ref.child( CommonProp.getUserSinPuntos() ).child('datos').child('seleccion').set(seleccion);
		//console.log(CommonProp.getSeleccion());
	};

}]);