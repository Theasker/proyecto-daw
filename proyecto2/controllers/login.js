var app = angular.module('masomenosApp.login', ['firebase']);

app.controller('LoginCtrl',['$scope','fbURL','CommonProp','$firebaseAuth','$state','$firebaseArray',function($scope,fbURL,CommonProp,$firebaseAuth,$state,$firebaseArray){
	var ref = new Firebase(fbURL);
	var loginObj = $firebaseAuth(ref);
	$scope.errorLogin = "";
	
	$scope.user = {};
	// ============================================================================

	// Login
	/*$scope.SignIn = function(e) {
		$scope.showErrorLogin = false;

		ref.authWithPassword({
		  email    : $scope.user.email,
		  password : $scope.user.password
		}, autenticacion) // Función del control de autenticación
	};*/

	$scope.SignIn = function(){
		loginObj.$authWithPassword({
			email    : $scope.user.email,
		  password : $scope.user.password
		}).then(function(authData){ // Login correcto
			//console.log("Autenticado correctamente:", authData);
	    // Guardo en un Factory los datos del usuario para poder usarlos después =======
	    if (loginObj.$getAuth()){
				var usuario = loginObj.$getAuth().password.email;
				CommonProp.setUser(usuario);
				//$scope.user.email = usuario;
			}
	    $scope.showErrorLogin = "false"; // Oculta el mensaje de error de login
	    $scope.errorLogin = "";
			//CommonProp.setUser(datosAutenticacion.password.email);
			CommonProp.setUser($scope.user.email);
			$state.go('home')
		}).catch(function(error){ // Login erróneo
			$scope.showErrorLogin = true;
			$scope.errorLogin = "";
  		switch(error.code) {
				case "INVALID_USER":
					$scope.errorLogin = 'Email incorrecto';
					break;
				case "INVALID_PASSWORD":
					$scope.errorLogin = 'Password incorrecto';
					break;
				case "INVALID_EMAIL":
					$scope.errorLogin = 'Email inválido';
					break;
				default: $scope.errorLogin = error;
			};
			console.log($scope.errorLogin,$scope.showErrorLogin)
		});
	};


		// Control del login 
	/*function autenticacion(error, datosAutenticacion){
		if (error) { // Login erróneo
			$scope.showErrorLogin = true;
			$scope.errorLogin = "";
  		switch(error.code) {
				case "INVALID_USER":
					$scope.errorLogin = 'Email incorrecto';
					break;
				case "INVALID_PASSWORD":
					$scope.errorLogin = 'Password incorrecto';
					break;
				case "INVALID_EMAIL":
					$scope.errorLogin = 'Email inválido';
					break;
				default: $scope.errorLogin = error;
			};
			console.log($scope.errorLogin,$scope.showErrorLogin)
	  } else { // Login correcto
	    console.log("Autenticado correctamente:", datosAutenticacion);
	    // Guardo en un Factory los datos del usuario para poder usarlos después =======
	    if (loginObj.$getAuth()){
				var usuario = loginObj.$getAuth().password.email;
				CommonProp.setUser(usuario);
				//$scope.user.email = usuario;
			}
	    $scope.showErrorLogin = "false"; // Oculta el mensaje de error de login
	    $scope.errorLogin = "";
			//CommonProp.setUser(datosAutenticacion.password.email);
			CommonProp.setUser($scope.user.email);
			$state.go('home');
	  }
	}*/

	$scope.Register = function(e){
		if (!$scope.signinForm.$invalid) {
			var email = $scope.user.email;
			var password = $scope.user.password;

			if (email && password){ // Comprobamos que se han introducido los datos
				loginObj.$createUser({
					email: email,
					password: password
				}).then(function(userData){ // Se ha creado con éxito
					console.log('Se ha creado el usuario ' + email + ' con uid: ' +userData.uid);
					$scope.SignIn();
				}).catch(function(error){ // No se ha podido crear el usuario
					console.log(error);
					// Mostramos el error
					$scope.showErrorLogin = true;
					$scope.errorLogin = error.message;
				});
			}
    }
	};
	
}]);