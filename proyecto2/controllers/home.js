var app = angular.module('masomenosApp.home', ['firebase']);

app.controller('HomeCtrl',['$scope','fbURL','$stateParams','CommonProp','$firebaseAuth','$firebaseArray','$firebaseObject','$state','currentAuth',function($scope,fbURL,$stateParams,CommonProp,$firebaseAuth,$firebaseArray,$firebaseObject,$state,currentAuth) {

	var ref = new Firebase(fbURL);
	var seleccion = {}; // Selección de año, mes y semana guardada en la base de datos.
	//var gastosImportes = {}; // Los gastos de cada día (pueden ser varios).
  //var gastosConceptos=[]; // Conceptos de gastos
  var usuarioSinPuntos = CommonProp.getUserSinPuntos(); // Usuario sin puntos
	var arrayDia=[
    {id:0, dia:"Domingo"},
    {id:1, dia:"Lunes"},
    {id:2, dia:"Martes"},
    {id:3, dia:"Miercoles"},
    {id:4, dia:"Jueves"},
    {id:5, dia:"Viernes"},
    {id:6, dia:"Sábado"}    
  ];
  var arrayMes=[
    {id:0, mes:"Enero"},
    {id:1, mes:"Febrero"},
    {id:2, mes:"Marzo"},
    {id:3, mes:"Abril"},
    {id:4, mes:"Mayo"},
    {id:5, mes:"Junio"},
    {id:6, mes:"Julio"},
    {id:7, mes:"Agosto"},
    {id:8, mes:"Septiembre"},
    {id:9, mes:"Octubre"},
    {id:10, mes:"Noviembre"},
    {id:11, mes:"Diciembre"}    
  ];
  
  //console.log($stateParams);
  //console.log(CommonProp.getSeleccion());
  $scope.dias = {};

  $scope.dias.showErrorSeleccion = false;

  console.log('estado días: ',$stateParams);

	if ($stateParams.anio){ // Si se ha seleccionado una semana y estamos en el estado 'dias'
		seleccion = {
			anio: $stateParams.anio, 
			mes: arrayMes[$stateParams.mes], 
			semana: $stateParams.semana
		};
		cargarSeleccion(seleccion);
	}
	else { // No se ha seleccionado nada 
		console.log("cargando seleccion de la bbdd");
		seleccionObj = $firebaseObject(ref.child( usuarioSinPuntos ).child('datos').child('seleccion'));
    seleccionObj.$loaded().then(function(x){
      if (seleccionObj.anio){ // Hay una seleccion guardada den la bbdd
        cargarSeleccion(seleccionObj); // cargamos la selección
        //console.log(seleccionObj);
      }else{ // No se ha seleccionado nada, ni hay una selección guardada en la base de datos.
        console.log("No existe selección en ningún lugar");
        $scope.dias.showDias = false;
        $scope.dias.showMensaje = true;
        $scope.dias.mensaje = 'Selecciona una semana del panel lateral izquierdo.';
        $scope.dias.mostrarTodo = false;

      }
    });
	}
	
  cargarGastos(); // Cargamos los conceptos de gastos gastos
  
  /* Cargamos la selección pasada como parámetro:
    { anio: "2015",
      mes: {id: 4,mes: "Mayo"},
      semana: "2"}
  */
	function cargarSeleccion(sel){
		//console.log(sel);
		//console.log(sel.anio,sel.mes,sel.semana);
		$scope.dias.semana = $firebaseObject(ref.child(usuarioSinPuntos).child('years').child(sel.anio).child(sel.mes.id).child(sel.semana));
    $scope.dias.showDias = true;
    $scope.dias.showMensaje = false;
    $scope.dias.showGastos = false;
    diasSemana(sel.anio,sel.mes.id,sel.semana);
    $scope.dias.mostrarTodo = true;
	}

  // Muestra el div de gastos
  $scope.dias.mostrarGastos = function(){
    $scope.dias.showDias = true;
    $scope.dias.showMensaje = false;
    $scope.dias.showGastos = true;
    $scope.dias.showGastosBtn = false;
  };

  // Oculta el div de gastos
  $scope.dias.ocultarGastos = function(){
    $scope.dias.showDias = true;
    $scope.dias.showMensaje = false;
    $scope.dias.showGastos = false;
    $scope.dias.showGastosBtn = true;
  };

  // 
  function cargarGastos(){
    $scope.dias.gastosConceptos = $firebaseArray(ref.child(usuarioSinPuntos).child('datos').child('gastos'));
    $scope.dias.gastosConceptos.$loaded().then(function(x){
      $scope.dias.showGastosBtn = true;
      /*if($scope.dias.gastosConceptos.length === 0){ // No hay ningún concepto de gasto
        console.log('No hay conceptos de gasto dados de alta');
      }else{
        console.log("hay gastos: ",$scope.dias.gastosConceptos);
      }*/
    }).catch(function(error){console.log('NO se han podido cargar los conceptos de gastos.');});
  }

  // Creación de un nuevo concepto de gasto
  $scope.dias.nuevoGastoConcepto = function(gastoConcepto){
    // Compruebo que el gasto no está duplicado
    var duplicado = false;
    for (var cont = 0;cont < $scope.dias.gastosConceptos.length;cont++){
      if($scope.dias.gastosConceptos[cont].$value == $scope.dias.gastoConcepto){
        duplicado = true;
      }
    }
    if (duplicado){ // Gasto duplicado
      $scope.dias.showGastoMensaje = true;
      $scope.dias.gastoMensaje = "Gasto duplicado.";
      $scope.dias.gastoConcepto = "";
    }else{ // Gasto no duplicado
      $scope.dias.showGastoMensaje = false;
      $scope.dias.gastosConceptos.$add(gastoConcepto);
      $scope.dias.gastoConcepto = ""; // Input del formulario
    }
  };

  // Eliminación de un concepto de gasto
  $scope.dias.borrarGastoConcepto = function(idGasto){
    $scope.dias.gastosConceptos.$remove(idGasto);
  };

  // Creación del arbol de días de la semana
  function diasSemana(anio,mes,semana){
  	// Guardo en el $scope los datos de anio, mes y semana seleccionados para usarlos posteriormente
  	$scope.dias.anio = anio;
  	$scope.dias.mes = mes;
  	$scope.dias.numsemana = semana;
    var desdeHasta = $firebaseObject(ref.child(usuarioSinPuntos).child('years').child(anio).child(mes).child(semana));
    desdeHasta.$loaded().then(function(x){
      if(desdeHasta.dias){ // Se ha creado ya los datos de los días.
        console.log("Ya había días creados en esta semana");
      }else { // Aun no se han creado los datos de los días
        var dias = crearDias(desdeHasta.desde,desdeHasta.hasta);
        console.log("No había días creados en esta semana: ",dias);
        ref.child(usuarioSinPuntos).child('years').child(anio).child(mes).child(semana).child('dias').set(dias);
      }

      $scope.dias.dias = $firebaseArray(ref.child(usuarioSinPuntos).child('years').child(anio).child(mes).child(semana).child('dias'));
      totales();
    });    
  }

  // Crea los días de la semana
  function crearDias(desde,hasta){
    var dias = [];
    //var dia = {};
    //var fecha = "";
    desde = new Date(desde); // Obtengo la fecha del timestamp de desde
    //hasta = new Date(hasta); // Obtengo la fecha del timestamp de hasta
    var hoydia = desde.getDate(); // Date del día desde que iré sumando durante la semana
    console.log('hasta: ',hasta);
    var hoyts = desde;
    console.log(hoyts.getTime());

    while (hoyts.getTime() <= hasta){
      //fecha = (hoyts.getDate()+"/"+hoyts.getMonth()+"/"+hoyts.getFullYear());
      dias.push( {dia: hoyts.getTime(), ingreso: 0.00, totalGastos: 0.00} );
      hoydia++;
      hoyts.setDate(hoydia);
      hoydia = hoyts.getDate();
    }
    return dias;
  }

  // Reinicio de los ingresos y gastos de un día de la semana
  $scope.dias.resetDia = function(id){
    var refobjDia = ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(id);
    var objDia = $firebaseObject(refobjDia);

    objDia.$loaded().then(function(x){
      objDia.ingreso = 0;
      objDia.gastos = {};
      objDia.totalGastos = 0;
      objDia.$save().then(function(refobjDia){
        totales();
      })
    })
  }

  // Actualizacion de un ingreso
  $scope.dias.nuevoIngreso = function(ingreso,id){
    var refobjDia = ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(id);
    var objDia = $firebaseObject(refobjDia);

    objDia.$loaded().then(function(x){
      objDia.ingreso = ingreso;
      objDia.$save().then(function(refobjDia){
        totales();
      })
    })
  }

  // Añade un gasto
  $scope.dias.nuevoGasto = function(id){
  	console.log("dias.gastoSeleccionado2: ",$scope.dias.gastoSeleccionado2[id]);
  	console.log('dias.gasto: ',$scope.dias.gasto[id]);
  	var refGasto = $firebaseArray(ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(id).child('gastos'));
  	refGasto.$loaded().then(function(x){
  		// $value: "Pan rallado", $id: "-JqpJjSIDO4pdX2u9_fa"
  		var gasto = {
  			concepto: $scope.dias.gastoSeleccionado2[id].$value,
  			//id: $scope.dias.gastoSeleccionado2[id].$id,
  			importe: $scope.dias.gasto[id]
  		};
      
      // Compruebo si está el total de Gastos
      var totalGasto = $firebaseObject(ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(id));
      totalGasto.$loaded().then(function(x){
        console.log(totalGasto);
        if (totalGasto.totalGastos){ // Si existe el total de gastos (si hay algun gasto tiene que estar el total)
          var total = totalGasto.totalGastos + gasto.importe;
          ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(id).child('totalGastos').set(total);
          console.log('existen gastos ',totalGasto.totalGastos);
        }else{
          ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(id).child('totalGastos').set(gasto.importe);
          console.log('no existe totalGastos');
        }
      });
      

  		refGasto.$add(gasto).then(function(x){
  			console.log('Se ha añadido el gasto');
  			// Ponemos el formulario vacío por si se quiere añdir otro gasto.
  			$scope.dias.gastoSeleccionado2[id] = "";
  			$scope.dias.gasto[id] = "";
  			$scope.dias.vistaGasto = [];
  			$scope.dias.vistaGasto[id] = refGasto;
  			totales();
  		}).catch(function(error){console.log('No se ha podido añadir el gasto.');});
  	}).catch(function(error){
  		console.log('No se ha podido cargar el arbol de gasto.');
  	});
  };

  // Borra un gasto
  $scope.dias.borrarGasto = function(iddia,idgasto){
    //var refGasto = $firebaseArray(ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(id).child('gastos').child(id));
    var refGasto = $firebaseObject(ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(iddia).child('gastos').child(idgasto));
    refGasto.$loaded().then(function(x){
      var gasto = refGasto.importe; // Guardo el importe para poder restarlo del total de gastos
      refGasto.$remove().then(function(x){ // Se ha borrado el gasto correctamente
        // Cargo el total de gastos para restarle el gasto eliminado
        var totalGasto = $firebaseObject(ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(iddia));
        totalGasto.$loaded().then(function(x){
          gasto = totalGasto.totalGastos - gasto;
          ref.child(usuarioSinPuntos).child('years').child($scope.dias.anio).child($scope.dias.mes).child($scope.dias.numsemana).child('dias').child(iddia).child('totalGastos').set(gasto);
        }).catch(function(error){console.log(error);});
        console.log('Se ha borrado bien el gasto');
        totales();
      }).catch(function(error){
        console.log('error:',error);
      });
    }).catch(function(error){console.log(error);});
  };

  $scope.dias.totales = function(){
    totales();
  }

	// Totales de ingresos y gastos por semana
	function totales(){
		var ingresos = 0;
		var gastos = 0;
    //console.log($scope.dias.semana.dias) 
    // Si se han creado los días de la semana
    if ($scope.dias.semana.dias != undefined){ 
      for (var i = 0;i < $scope.dias.semana.dias.length;i++){
        if ($scope.dias.semana.dias[i].ingreso){
          ingresos = ingresos + $scope.dias.semana.dias[i].ingreso;
        }

        if ($scope.dias.semana.dias[i].gastos){ // Si hay algún gasto
          for (var indice in $scope.dias.semana.dias[i].gastos) {
            gastos = gastos + $scope.dias.semana.dias[i].gastos[indice].importe;
          }
        }else if($scope.dias.semana.dias[i].totalGastos != 0){ 
          // NO hay ningún gasto pero si el total gastos = generadas las estadísticas automáticas.
          gastos = gastos + $scope.dias.semana.dias[i].totalGastos;
        }
      }
    }else{ //No se han creado los días de la semana
      ingresos = 0;
      gastos = 0;
    }
		
    $scope.dias.totalIngresos = ingresos;
    $scope.dias.totalGastos = gastos;
		//console.log('ingresos: ',ingresos,' gastos: ',gastos);
	}

}]);