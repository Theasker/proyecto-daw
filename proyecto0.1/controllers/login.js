angular.module('masomenosApp.login', ['firebase'])

.controller('LoginCtrl',['$scope','fbURL','CommonProp','$firebaseAuth','$state','$firebaseArray',
	function($scope,fbURL,CommonProp,$firebaseAuth,$state,$firebaseArray) {
	var referencia = new Firebase(fbURL);
	var loginObj = $firebaseAuth(referencia);
	$scope.user = {};
	// Guardo en un Factory los datos del usuario para poder usralos después =======
	if (loginObj.$getAuth()){
		var usuario = loginObj.$getAuth().password.email;
		CommonProp.setUser(usuario);
		$scope.user.email = usuario;
	}
	// ============================================================================
	$scope.SignIn = function(e) {
		$scope.errorLogin = false;
		$scope.regError = false;
		//e.preventDefault();
		var username = $scope.user.email;
		var password = $scope.user.password;
		
		referencia.authWithPassword({
	    "email": $scope.user.email,
	    "password": $scope.user.password
	  }, function(error, authData) {
	    if (error) { // Login erroneo
	    	console.log(error.code,$scope);
	    	$scope.errorLogin = true; // Muestra el mensaje de error de login
				$scope.regError = true;
    		switch(error.code) {
					case "INVALID_USER":
						$scope.regErrorMessage = 'Email incorrecto';
						break;
					case "INVALID_PASSWORD":
						$scope.regErrorMessage = 'Password incorrecto';
						break;
					case "INVALID_EMAIL":
						$scope.regErrorMessage = 'Email inválido.';
						break;
					default: $scope.msg = error;
				}
	    } else { // Login correcto
	      console.log("Autenticado correctamente: ", authData);
	      $scope.errorLogin = "false"; // Oculta el mensaje de error de login
				$state.go('home');
				CommonProp.setUser(username);
	    }
	  });
	};

	$scope.Logout = function(){
		referencia.unauth();
		$state.go("login");
	};

	$scope.Register = function(e){
		if (!$scope.signinForm.$invalid) {
			var email = $scope.user.email;
			var password = $scope.user.password;
			if (email && password){
				loginObj.$createUser({
					email: email,
					password: password
				}).then(function(userData){ // Se ha creado con éxito
					console.log('Se ha creado el usuario ' + email + ' con uid: ' +userData.uid);
					$scope.SignIn();
				}).catch(function(error){ // No se ha podido crear el usuario
					console.log(error);
					// Mostramos el error
					$scope.regError = true;
					$scope.regErrorMessage = error.message;
				});
			}
    }
	};
}]);