var app = angular.module('masomenosApp.fechas', ['firebase']);
app.controller("FechasCtrl", ['$scope','fbURL','$state','CommonProp','$firebaseArray','$firebaseObject','$firebaseAuth','currentAuth',function($scope,fbURL,$state,CommonProp,$firebaseArray,$firebaseObject,$firebaseAuth,currentAuth) {
	
	var ref = new Firebase(fbURL);

	var autenticado = $firebaseAuth(ref).$getAuth();
	var usuarioSinPuntos = CommonProp.getUserSinPuntos();
	// Visualizaciones
	$scope.year = {};
	$scope.year.mensaje = "";

	CommonProp.setUser(autenticado.password.email);
	// Cargamos, si existe, los datos de seleccion de año, mes y semana.
	

	var arrayDia=[
    {id:0, dia:"Domingo"},
    {id:1, dia:"Lunes"},
    {id:2, dia:"Martes"},
    {id:3, dia:"Miercoles"},
    {id:4, dia:"Jueves"},
    {id:5, dia:"Viernes"},
    {id:6, dia:"Sábado"}    
  ];
	var arrayMes=[
    {id:0, mes:"Enero"},
    {id:1, mes:"Febrero"},
    {id:2, mes:"Marzo"},
    {id:3, mes:"Abril"},
    {id:4, mes:"Mayo"},
    {id:5, mes:"Junio"},
    {id:6, mes:"Julio"},
    {id:7, mes:"Agosto"},
    {id:8, mes:"Septiembre"},
    {id:9, mes:"Octubre"},
    {id:10, mes:"Noviembre"},
    {id:11, mes:"Diciembre"}    
  ];

	crearInicio();
	comprobarAnios();
	cargarSeleccion();

	// Comprobación/creación si el inicio del arbol de un usuario si está creado
	function crearInicio(){
		var inicioArbol = $firebaseArray(new Firebase(fbURL + usuarioSinPuntos + '/datos'));
		//$scope.datos = inicioArbol;
		inicioArbol.$loaded() // Promesa para saber cuando se ha cargado el arbol del usuario
		.then(function(x){
			if (inicioArbol.length === 0){ // No hay datos en el arbol de inicio del usuario
				//console.log('No hay datos del usuario '+ usuarioSinPuntos);
				var usuario = {
					email: autenticado.password.email,
					uid: autenticado.uid,
					token: autenticado.token
				};
				ref.child(usuarioSinPuntos).child('datos').set(usuario);
				//inicioArbol.$add(usuario); // Agregamos los datos del usuario
				console.log("Datos agregados");
			}
			//else console.log('Hay datos de usuario '+ usuarioSinPuntos);
		});
	}

	// Comprobamos si hay o no años dados de
	function comprobarAnios(){
		var treeYears = $firebaseArray(new Firebase(fbURL + usuarioSinPuntos + '/years'));
		$scope.year = {};
		$scope.year.mensaje = false; // Para mostrar o no mostrar mensajes
		$scope.year.year = ""; // Año introducido por el usuario
		//mostrarAnios();

		treeYears.$loaded().then(function(x){
			if(treeYears.length === 0){ // No hay datos en el arbol
				$scope.year.mensaje = "No hay ningún año para mostrar, añade uno:";
			}else { // Hay años agregados y los cargamos en el $scope para visualizarlos
				console.log('Comprobando años: Hay años agregados');
				$scope.year.anios = treeYears;
				mostrarAnios();
				console.log(treeYears);
			} 
		});
	}

	function cargarSeleccion(){
		var seleccion = $firebaseObject(ref.child( usuarioSinPuntos ).child('datos').child('seleccion'));
		seleccion.$loaded()
		.then(function(x){
			//console.log(seleccion);
			// Compruebo que hay datos
			if (seleccion.anio){
				CommonProp.setSeleccion(seleccion);
				$scope.year.anioSeleccionado = seleccion.anio;
				$scope.year.mesSeleccionado = seleccion.mes;
				$scope.year.dias = seleccion.semana;
				$scope.year.mostrarSemanas(seleccion.anio);
			}else{
				console.log('no hay datos de seleccion');
				mostrarAnios();
			}
		}).catch(function(error){
			console.log('NO se ha podido cargar la seleccion.');
		});
	}

	// Crea un nuevo año
	$scope.year.newYear = function(){
		console.log("newYear($scope.year.year)",$scope.year.year);
		var anio = $firebaseArray(new Firebase(fbURL + usuarioSinPuntos + '/years/' + $scope.year.year));
		// Comprobamos que el año dado no existe
		anio.$loaded().then(function(x){
			if(anio.length === 0){ // No existe ese año
				// Controlamos que no se introduzcan años antigüos
				console.log("newYear(anio.length): ",anio.length);
				if ($scope.year.year < 1970 ) { // Año demasiado viejo
					$scope.year.mensaje = "No se pueden introducir años antes de 1.970.";
					$scope.year.year = "";
				}else{
					console.log("El año "+$scope.year.year+" no existe.");
					// Añadimos el año generado
					anio = $firebaseArray(new Firebase(fbURL + usuarioSinPuntos + '/years/'));
					ref.child( CommonProp.getUserSinPuntos() ).child('years').child($scope.year.year).set( calculoSemanas($scope.year.year) );
					$scope.year.year = "";
					comprobarAnios();
				}
			}else { // Año duplicado
				console.log("El año "+$scope.year.year+" ya existe y no se puede duplicar.");
				$scope.year.mensaje = "El año "+$scope.year.year+" ya existe y no se puede duplicar.";
			} 
		});
	};

	// Crea las semanas de un año dado
	function calculoSemanas(anio){

		// Establecemos el primer día del año
		// Los parámetros son año, mes(0-11), día, horas, minutos, segundos (sólo usamos año, mes y día)
		dt = new Date(anio,0,1); // yyyy/mm/dd -> yyyy/01/01
	
		var dayweek = dt.getDay();
		var month = dt.getMonth();
		var day = dt.getDate();
		var year = dt.getFullYear();
		var semanas = new Array(); // Array con los datos de todas las semanas
		//var semanas = {year: year}; // Objeto con los datos de todas las semanas
		var desde = dt.getTime();
		var hasta;
		// Asigno el mes a otra variable para poder controlar en cambio del mismo
		mesAnterior = month; // Cambiamos de mes
		semanas.push(new Array()); // Agregamos el mes al array
		while(year == dt.getFullYear()){
			day++; // Sumo un día
			month = dt.getMonth(); // Obtengo el mes de esta fecha
			dt.setDate(day); // Asigno el día sumado 
			day = dt.getDate(); // Obtenemos el día de la fecha
			// Si el día es 1 (lunes) lo guardamos en 'desde'
			// para saber el día que comienza la semana
			if (dt.getDay() == 1){ // El día 1 es el lunes
				desde = dt.getTime();
			}
			// Si el día es 0 (domingo) o final de año ( 31/11 -> 11 es diciembre) lo guardamos en 'hasta' 
			// para saber el día final de semana
			if ( (dt.getDay() === 0) || ( (dt.getMonth() == 11) && (dt.getDate() == 31) ) ) {
				// Controlamos el cambio del mes
				if (month > mesAnterior){
					mesAnterior = month; // Cambiamos de mes
					semanas.push(new Array()); // Agregamos el mes al array
				}
				hasta = dt.getTime();
				//semanas.meses = {('mes'+month): {desde: desde, hasta: hasta}};
				// Creo una variable Date para poder sacar dd/mm/yyyy
				desde2 = new Date(desde);
				hasta2 = new Date(hasta);
				
				semanas[month].push({
					//desde: desde2.getDate()+"/"+desde2.getMonth()+"/"+desde2.getFullYear(),
					//hasta: hasta2.getDate()+"/"+hasta2.getMonth()+"/"+hasta2.getFullYear()
					desde: desde, hasta: hasta
				});
				
			}
		}
		return semanas;
	}
	
	$scope.year.mostrarAnios = function(){
		mostrarAnios();
	};
	
	function mostrarAnios(){
		$scope.year.showYears = true;
		$scope.year.showMeses = false;
		$scope.year.showSemanas = false;
	}
	
	// Funcion que borra un año seleccionado
	$scope.year.borrarAnio = function(anio){
		var ref = new Firebase(fbURL + usuarioSinPuntos + '/years/'+anio);
		var obj = $firebaseObject(ref);
		console.log(obj);
		obj.$remove().then(function(ref) {
		  // data has been deleted locally and in Firebase
		  console.log('El año '+anio+' ha sido borrado');
		  // Eliminar la selección de año, mes y semana si corresponde con el año a borrar
		  eliminarSeleccion(anio);
		  comprobarAnios();
		}, function(error) {
		  console.log("Error:", error);
		});
		

	};

	// Eliminar la selección de año, mes y semana si corresponde con el año a borrar
	function eliminarSeleccion(anio){
		var seleccion = $firebaseObject(ref.child( usuarioSinPuntos ).child('datos').child('seleccion'));
		seleccion.$loaded()
		.then(function(x){
			//console.log(seleccion);
			// Compruebo que hay datos y coincide con el año a borrar
			console.log("eliminarSeleccion: ",seleccion.anio,anio);
			if (seleccion.anio == anio){
				seleccion.$remove();
			}
		}).catch(function(error){console.log('NO se ha podido cargar la seleccion.');});
	}

	// Muestra los meses del año seleccionado
	$scope.year.mostrarMeses = function(anio){
		$scope.year.showYears = false;
		$scope.year.showMeses = true;
		$scope.year.showSemanas = false;

		console.log("mostrarMeses: ",anio);

		$scope.year.anioSeleccionado = anio; // Guardo el año en que se ha hecho click
		$scope.year.meses = arrayMes; // Asocio el objeto a los datos del select
	};
	
	$scope.year.mostrarSemanas = function(anio){
		mostrarSemanas(anio);
	};

	function mostrarSemanas(anio){
		$scope.year.showYears = false;
		$scope.year.showMeses = false;
		$scope.year.showSemanas = true;

		//ref.child('usuarios').child(usuarioSinPuntos).child('datos').set(usuario);
		//ref = new Firebase(fbURL + usuarioSinPuntos + '/years/');
		//ref.child(usuarioSinPuntos).child('years').child( $scope.year.anioSeleccionado ).child( $scope.year.mesSeleccionado )

		var ref = new Firebase(fbURL + usuarioSinPuntos + '/years/'+$scope.year.anioSeleccionado); 
		var semanas = $firebaseArray( ref.child($scope.year.mesSeleccionado.id) );

		$scope.year.semanas = semanas;
	}
	
	$scope.year.mostrarDias = function(idSemana){
		var seleccion = {
			anio: $scope.year.anioSeleccionado,
			mes: $scope.year.mesSeleccionado,
			semana: idSemana
		};
		$scope.year.seleccion = seleccion;
		CommonProp.setSeleccion(seleccion);
		
		$scope.year.dias = idSemana; // Semana seleccionada
		// Guardo la seleccion de año, mes y semana en la base de datos.
		ref.child( CommonProp.getUserSinPuntos() ).child('datos').child('seleccion').set(seleccion);
		//console.log(CommonProp.getSeleccion());
	};
}]);
