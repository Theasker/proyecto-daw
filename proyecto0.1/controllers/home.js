angular.module('masomenosApp.home', ['firebase'])

.controller('HomeCtrl',['$scope','fbURL','$stateParams','CommonProp','$firebaseAuth','$firebaseArray','$firebaseObject','$state','currentAuth',function($scope,fbURL,$stateParams,CommonProp,$firebaseAuth,$firebaseArray,$firebaseObject,$state,currentAuth) {

	var ref = new Firebase(fbURL);
	var seleccion = {}; // Selección de año, mes y semana guardada en la base de datos.
	var gastosImportes = {}; // Los gastos de cada día (pueden ser varios).
  var gastosConceptos=[]; // Conceptos de gastos
  var usuarioSinPuntos = CommonProp.getUserSinPuntos(); // Usuario sin puntos
	var arrayDia=[
    {id:0, dia:"Domingo"},
    {id:1, dia:"Lunes"},
    {id:2, dia:"Martes"},
    {id:3, dia:"Miercoles"},
    {id:4, dia:"Jueves"},
    {id:5, dia:"Viernes"},
    {id:6, dia:"Sábado"}    
  ];

	$scope.dias = {}; 
	$scope.dias.showMensaje = false;
	//console.log($stateParams);

	//comprobarAnios();

	// Compruebo los años que hay en la base de datos
	function comprobarAnios(){
		var treeYears = $firebaseArray(new Firebase(fbURL + usuarioSinPuntos + '/years'));
		treeYears.$loaded().then(function(x){
			if(treeYears.length !== 0){ // Hay datos en el arbol de años
				if($stateParams.anio){ // Se han pasado parámetros
					console.log("existen parámetros");
					cargarSeleccionParam(); 
				}else { // Sin parámetros
					console.log("NO existen parámetros");
					cargarSeleccion();
				}
			}
			cargarGastos(); // Cargo el select de los gastos
		});
	}

	// Cargo los datos seleccionados de año, mes y semana de la base de datos.
	function cargarSeleccion(){
		seleccion = $firebaseObject(ref.child( usuarioSinPuntos ).child('datos').child('seleccion'));
		seleccion.$loaded()
			.then(function(x){
				if (seleccion.anio){ // Compruebo que hay datos
					//console.log($scope.dias.seleccion); 
					$scope.dias.semana = $firebaseObject(ref.child(usuarioSinPuntos).child('years')
						.child(seleccion.anio)
						.child(seleccion.mes.id)
						.child(seleccion.semana));
					diasSemana(seleccion.anio, seleccion.mes.id, seleccion.semana); // Genero los días de la semana
				}else{console.log('no hay datos de seleccion');}
			}).catch(function(error){console.log('NO se ha podido cargar la seleccion.');});
	}

	// Cargo los datos seleccionados de año, mes y semana de la base de datos.
	// según los datos pasados como parámetros seleccionados en sidebar.
	function cargarSeleccionParam(){
		$scope.dias.semana = $firebaseObject(ref.child(usuarioSinPuntos).child('years')
			.child($stateParams.anio)
			.child($stateParams.mes)
			.child($stateParams.semana));
		diasSemana($stateParams.anio,$stateParams.mes,$stateParams.semana); // Genero los días de la semana
	}

	// Datos del select con los gastos
	function cargarGastos(){
		// Enlazo los "conceptos de gasto" con una variable del $scope y con firebase
		/*var obj = $firebaseObject(ref.child(usuarioSinPuntos).child('datos').child('gastos'));
		obj.$bindTo($scope,"dias.gastosConceptos").then(function(){

		})*/

		$scope.dias.gastosConceptos = $firebaseArray(ref.child(usuarioSinPuntos).child('datos').child('gastos'));
		$scope.dias.gastosConceptos.$loaded()
			.then(function(x){
				if($scope.dias.gastosConceptos.length === 0){ // No hay ningún concepto de gasto
					console.log('No hay conceptos de gasto dados de alta');
				}else{
					console.log("hay gastos: ",$scope.dias.gastosConceptos);
				}
			}).catch(function(error){console.log('NO se han podido cargar los conceptos de gastos.');});
	}

	// Creación de un nuevo concepto de gasto
	$scope.dias.nuevoGastoConcepto = function(gastoConcepto){
		// Compruebo que el gasto no está duplicado
		var duplicado = false;
		for (var key in $scope.dias.semana) {
			console.log(key);
		}

		if (duplicado){ // Gasto duplicado
			$scope.dias.showGastoMensaje = true;
			$scope.dias.gastoMensaje = "Gasto duplicado.";
		}else{ // Gasto no duplicado
			$scope.dias.showGastoMensaje = false;
			$scope.dias.gastosConceptos.$add(gastoConcepto);
			$scope.dias.gastoConcepto = ""; // Input del formulario
		}
	};

	// Eliminación de un concepto de gasto
	$scope.dias.borrarGastoConcepto = function(idGasto){
		$scope.dias.gastosConceptos.$remove(idGasto);
	};

	// Creación del arbol de días de la semana
	function diasSemana(anio,mes,semana){
		var desdeHasta = $firebaseArray(ref.child(usuarioSinPuntos).child('years').child(anio).child(mes).child(semana));
		for(var i = 0;i < desdeHasta.length;i++){
			//console.log(desdeHasta[i]);
		}
		var dias = [
			{ingreso: 30, gastos:[{nombre: "salchichas", importe: 36.3}]},
			{ingreso: 40, gastos:[{nombre: "legía", importe: 6.0}]}
			];
		console.log(anio,mes,semana);
		//ref.child(usuarioSinPuntos).child('years').child(anio).child(mes).child(semana).child(dias).set(["34","53","quetepires"]);
		//$scope.year.semanas = semanas;
		var rango = $scope.dias.semana;
		console.log(rango);
		var desde = $scope.dias.semana.desde;
		var hasta = $scope.dias.semana.desde;
		
		//var dia = desde.getDate();
		console.log($scope.dias.semana.desde);
		console.log(desde);
	}
}]);
