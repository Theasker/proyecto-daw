var autenticado = referencia.getAuth();

if (autenticado){ //usuario autenticado
	console.log("Usuario " + CommonProp.getUser()+" autenticado.")
	$state.go('home.loginbien');
}else{ // usuario no autenticado
	console.log("Ningun usuario autenticado");
	$state.go('login');
}



.run(["$rootScope", "$state", function($rootScope, $state) {
	$rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
		/*
		event – {Object} – Event object.
		toState – {State} – The state being transitioned to.
		toParams – {Object} – The params supplied to the toState.
		fromState – {State} – The current state, pre-transition.
		fromParams – {Object} – The params supplied to the fromState.
		error – {Error} – The resolve error object.
		*/
	  // We can catch the error thrown when the $requireAuth promise is rejected
	  // and redirect the user back to the home page
	  if (error === "AUTH_REQUIRED") {
	  	$state.go('login');
	  }
	});
}])

function calculoSemanas(anio){

		// Establecemos el primer día del año
		// Los parámetros son año, mes(0-11), día, horas, minutos, segundos (sólo usamos año, mes y día)
		dt = new Date(anio,0,1); // yyyy/mm/dd -> yyyy/01/01
	
		var dayweek = dt.getDay();
		var month = dt.getMonth();
		var day = dt.getDate();
		var year = dt.getFullYear();
		var semanas = new Array(); // Array con los datos de todas las semanas
		var desde = dt.getTime();
		var hasta;
		// Asigno el mes a otra variable para poder controlar en cambio del mismo
		mesAnterior = month; // Cambiamos de mes
		semanas.push(new Array()); // Agregamos el mes al array
		while(year == dt.getFullYear()){
			day++; // Sumo un día
			month = dt.getMonth();
			dt.setDate(day); // Asigno el día sumado 
			day = dt.getDate(); // Obtenemos el día de la fecha
			// Si el día es 1 (lunes) lo guardamos en 'desde'
			// para saber el día que comienza la semana
			if (dt.getDay() == 1){ // El día 1 es el lunes
				desde = dt.getTime();
			}
			// Si el día es 0 (domingo) o final de año ( 31/11 -> 11 es diciembre) lo guardamos en 'hasta' 
			// para saber el día final de semana
			if ( (dt.getDay() === 0) || ( (dt.getMonth() == 11) && (dt.getDate() == 31) ) ) {
				// Controlamos el cambio del mes
				if (month > mesAnterior){
					mesAnterior = month; // Cambiamos de mes
					semanas.push(new Array()); // Agregamos el mes al array
				}
				hasta = dt.getTime();
				// Creo una variable Date para poder sacar dd/mm/yyyy
				desde2 = new Date(desde);
				hasta2 = new Date(hasta);
				semanas[month].push({
					//desde: desde2.getDate()+"/"+desde2.getMonth()+"/"+desde2.getFullYear(),
					//hasta: hasta2.getDate()+"/"+hasta2.getMonth()+"/"+hasta2.getFullYear()
					desde: desde, hasta: hasta
				});
			}
		}
		return semanas;
	}